export enum Status {
    Ok                  = 10,
    NotInSession        = 20,
    ItemNotEditable     = 21,
    ItemNotSelected     = 22,
    AlreadyInSession    = 23,
    InvitationNotFound  = 24,
    JoinRequestNotFound = 25,
    ItemNotFound        = 26,
    NotAuthorized       = 30,
    UserNotFound        = 40,
    SessionNotFound     = 41
}