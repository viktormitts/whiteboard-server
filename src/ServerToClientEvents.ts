import { Item, Vec2 } from './Item';
import User from './User';
import { Status } from './Status';

interface ServerToClientEvents {
    requestFailed: (message: string, status: Status) => void,

    // Starting a session
    /**
     * Server tells the client who requested to start a session that the session has been created.
     * @param sessionID The ID of the session.
     */
    sessionStarted: (sessionID: string) => void,

    // Inviting users
    /**
     * Server sends a list of available users to the host.
     * @param users Users
     */
    availableUsers: (users: User[]) => void,
    /**
     * Server notifies the host that an invitation has been sent.
     * @param inviteeID The ID of the invited user.
     */
    invitationSent: (inviteeID: string) => void,
    /**
     * Server forwards an invitation to the specified user.
     * @param sessionID The ID of the session.
     */
    sessionInvitation: (sessionID: string, host: User) => void,

    // Requesting to join a session
    /**
     * Server forwards a join request to the host.
     * @param The ID of the user who requested to join.
     */
    receiveJoinRequest: (requestor: User) => void,
    /**
     * The server tells the user that it has received their join request and has sent it to the host.
     * @param sessionID The ID of the session.
     */
    awaitingHostResponse: (sessionID: string) => void,
    /**
     * Server notifies the user that their join request was rejected.
     * @param sessionID The ID of the session the user requested to join.
     */
    joinRequestDenied: (sessionID: string) => void,

    // Connecting to a session
    /**
     * The server tells the user they have connected to the session.
     * @param sessionID The session
     */
    connectedToSession: (sessionID: string) => void,
    /**
     * The server tells session participants that a new user has joined the session.
     * @param username The name of the user who joined.
     */
    userJoinedSession: (username: string) => void,

    // Ending a session
    /**
     * Server notifies the user that the session ended.
     * @param sessionID The session that has ended.
     */
    sessionEnded: (sessionID: string) => void,

    // Whiteboard
    /**
     * The server sends the current state of the whiteboard to a newly connected user.
     * @param items The items currently on the whiteboard.
     */
    whiteboard: (items: Item[]) => void,
    /**
     * Server forwards an item to a user.
     * @param item The item
     * @param id The ID of the item
     */
    item: (item: Item, id: string) => void,
    /**
     * Server sends the deletion to users.
     * @param itemID The item
     */
    itemDeleted: (itemID: string) => void,
    /**
     * Server denies a user request.
     * @param message Message explaining why the request was denied.
     */
    requestDenied: (message: string) => void,
    /**
     * Server informs users that an item (note) has been selected and is locked from editing.
     * @param itemID the item
     * @param userID the user
     */
    itemSelected: (itemID: string, userID: string) => void,
    /**
     * Server informs users that an item (note) has been unselected and is free for editing.
     * @param itemID the item
     */
    itemReleased: (itemID: string) => void,
    /**
     * Server sends the new note position to a user.
     * @param itemID The ID of the item
     * @param position The new position
     */
    notePosition: (itemID: string, position: Vec2) => void,
    /**
     * Server sends the new note text to a user.
     * @param itemID The ID of the note
     * @param text The new text
     */
    noteText: (itemID: string, text: string) => void

    updateDrawing: (itemID: string, edge: [Vec2, Vec2]) => void
}

export default ServerToClientEvents;