import { Client } from "socket.io/dist/client";
import { Item, Vec2 } from './Item';
import { Status } from './Status';

interface AddItemResponse {
    status: Status,
    id: string
}

interface SelectItemResponse {
    granted: boolean,
    user: string
}

interface ClientToServerEvents {
    /**
     * Client informs the server of its username. Username does not have to be unique.
     * @param username The client's username.
     */
    username: (username: string) => void,

    // Starting a session
    /**
     * Client requests to host a new session.
     */
    startSession: () => void,

    // Inviting users
    /**
     * Host tells the client to send a list of available users, excluding those already in the session.
     */
    getAvailableUsers: () => void,
    /**
     * Host invites a user.
     * @param recipientID The recipient user.
     */
    inviteUser: (recipientID: string) => void,
    /**
     * User accepts an invitation.
     * @param sessionID The session the user is invited to.
     */
    acceptInvitation: (sessionID: string) => void,

    // Requesting to join a session
    /**
     * User requests to join a session.
     * @param sessionID The session the user wishes to join.
     */
    sendJoinRequest: (sessionID: string) => void,
    /**
     * Host accepts a join request.
     * @param requestorID The user who is accepted.
     */
    acceptJoinRequest: (requestorID: string) => void,
    /**
     * Host rejects a join request.
     * @param requestorID The user who is rejected.
     */
    denyJoinRequest: (requestorID: string) => void,

    // Ending a session
    /**
     * Host ends a session.
     */
    endSession: () => void,

    // Whiteboard
    /**
     * User adds an item (drawing, sticky note, image, or comment) to the whiteboard.
     * @param item The added item
     */
    addItem: (item: Item, callback: (response: AddItemResponse) => void) => void,
    /**
     * User deletes an item (erases a drawing or deletes a note).
     * @param itemID The ID of the item
     */
    deleteItem: (itemID: string) => void,
    /**
     * User selects a sticky note or comment.
     * @param itemID The selected item
     */
    selectItem: (itemID: string, callback: (response: SelectItemResponse) => void) => void,
    /**
     * User moves a sticky note.
     * @param itemID The note
     * @param position The new position
     */
    moveNote: (itemID: string, position: Vec2) => void,
    /**
     * User edits a sticky note or comment.
     * @param itemID the note or the comment
     * @param text The new text
     */
    editItem: (itemID: string, text: string) => void,
    /**
     * User stops moving or editing a note.
     * @param itemID The ID of the note
     */
    releaseNote: (itemID: string) => void,
    /**
     * User undoes a drawing.
     * @param itemID The ID of the drawing? What if you want to undo an erasure? Right now this is just the same as deleteItem.
     */
    undoDrawing: (itemID: string) => void,

    updateDrawing: (itemID: string, edge: [Vec2, Vec2]) => void
}

export default ClientToServerEvents;