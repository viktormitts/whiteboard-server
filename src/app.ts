import { Request, Response } from 'express'
import { Server, Socket, RemoteSocket } from 'socket.io'
import ServerToClientEvents from './ServerToClientEvents'
import ClientToServerEvents from './ClientToServerEvents'
import { Status } from './Status';
import { Item, ItemType, Note, Drawing, Comment } from './Item'
import { DefaultEventsMap } from 'socket.io/dist/typed-events'

const express = require('express')
const app = express()
const http = require('http')
const { subtle } = require('crypto').webcrypto;
const server = http.createServer(app)
const io = new Server<ClientToServerEvents, ServerToClientEvents>(server, { cors: { origin: true } })

const port = process.env.PORT || 3000

/**
 * Maps user IDs to usernames
 */
const usernames: Map<string, string> = new Map();

const whiteboards: Map<string, Map<string, Item>> = new Map();

const joinRequests: Map<string, string> = new Map();
const invitations: Map<string, string> = new Map();

app.use(express.static(__dirname + '/public'))

app.get('/', (req: Request, res: Response) => {
  res.sendFile(__dirname + '/public/index.html')
})

function sessionToUser(sessionID: string): string {
  return sessionID.slice(0, sessionID.length - 1);
}

function userToSession(userID: string): string {
  return userID + 's';
}

/**
 * Join a user to a session.
 * @param user The user's socket
 * @param session The session ID
 * @throws Error if the session does not exist.
 */
function joinSession(user: Socket<ClientToServerEvents, ServerToClientEvents, DefaultEventsMap, any> | RemoteSocket<ServerToClientEvents,any>, session: string) {
  if (whiteboards.has(session)) {
    user.join(session);
    user.emit('connectedToSession', session);
    user.emit('whiteboard', Array.from(whiteboards.get(session).values()));
  }
  else throw new Error(`Attempted to join user ${user.id} to session ${session}, which does not exist`);
}

/**
 * Gets a user's session. Assumes the user is already in a session.
 * @param socket The user's socket.
 * @returns The ID of the session the user is in.
 */
function getUserSession(socket: Socket): string {
  // oh no
  const iter = socket.rooms.values();
  // dear god
  iter.next();
  // stop
  return iter.next().value;
}

async function endSession(id: string) {
  io.to(id).emit('sessionEnded', id);
  for (const user of (await io.in(id).fetchSockets())) {
    user.leave(id);
  }
  whiteboards.delete(id);
  for (const request of joinRequests) {
    if (request[1] == id) {
      joinRequests.delete(request[0]);
    }
  }
  for (const invitation of invitations) {
    if (invitation[1] == id) {
      joinRequests.delete(invitation[0]);
    }
  }
}

async function leaveSession(user: Socket) {
  const sessionID = userToSession(user.id);
  if (user.rooms.has(sessionID)) {
    await endSession(sessionID);
  } else {
    user.leave(getUserSession(user));
  }
}

io.on('connection', (socket) => {
  console.log('A user connected');
  // default name
  if (!usernames.has(socket.id)) {
    usernames.set(socket.id, "Anonymous");
  }

  socket.on('username', username => {
    usernames.set(socket.id, username);
  })

  socket.on('startSession', () => {
    const sessionID = userToSession(socket.id);
    socket.join(sessionID);
    whiteboards.set(sessionID, new Map());
    socket.emit('sessionStarted', sessionID);
  })

  socket.on('sendJoinRequest', sessionID => {
    socket.to(sessionToUser(sessionID)).emit('receiveJoinRequest', {id: socket.id, username: usernames.get(socket.id)});
    socket.emit('awaitingHostResponse', sessionID);
    joinRequests.set(socket.id, sessionID);
  })

  socket.on('acceptJoinRequest', async requestorID => {
    const maybeRequestor = (await io.in(requestorID).fetchSockets());
    if (maybeRequestor.length < 1) {
      socket.emit('requestFailed', `User ${requestorID} does not exist`, Status.UserNotFound);
    }
    else {
      const sessionId = userToSession(socket.id);
      if (joinRequests.has(requestorID) && joinRequests.get(requestorID) == sessionId) {
        const requestor = maybeRequestor[0]
        try {
          joinSession(requestor, sessionId);
          joinRequests.delete(requestorID);
        }
        catch (e) {
          socket.emit('requestFailed', "acceptJoinRequest failed. You must host a session to accept invitations.", Status.NotAuthorized);
          console.error(e);
        }
      }
      else {
        socket.emit('requestFailed', `User ${requestorID} has not requested to join your session`, Status.JoinRequestNotFound);
      }
    }
  })

  socket.on('denyJoinRequest', requestorID => {
    if (joinRequests.has(requestorID) && joinRequests.get(requestorID) == userToSession(socket.id)) {
      joinRequests.delete(requestorID);
      socket.to(requestorID).emit('joinRequestDenied', userToSession(socket.id));
    }
    else {
      socket.emit('requestFailed', `User ${requestorID} has not requested to join your session`, Status.JoinRequestNotFound);
    }
  })

  socket.on('getAvailableUsers', async () => {
    const users = (await io.fetchSockets()).filter(x => !x.rooms.has(getUserSession(socket))).map(x => ({id: x.id, username: usernames.get(x.id)}));
    socket.emit('availableUsers', users);
  })

  socket.on('inviteUser', recipientID => {
    if (whiteboards.has(userToSession(socket.id))) {
      socket.to(recipientID).emit('sessionInvitation', userToSession(socket.id), {id: socket.id, username: usernames.get(socket.id)});
      socket.emit('invitationSent', recipientID);
      invitations.set(recipientID, userToSession(socket.id));
    }
    else {
      socket.emit('requestFailed', "You must host a session to invite users", Status.NotAuthorized);
    }
  });

  socket.on('acceptInvitation', sessionID => {
    if (invitations.has(socket.id) && invitations.get(socket.id) == sessionID) {
      try {
        joinSession(socket, sessionID);
        invitations.delete(socket.id);
      }
      catch (e) {
        socket.emit('requestFailed', "Session does not exist", Status.SessionNotFound);
        console.error(e);
      }
    }
    else {
      socket.emit('requestFailed', "You have not been invited", Status.InvitationNotFound);
    }
  })

  socket.on('addItem', (item, callback) => {
    if (socket.rooms.size == 1) {
      callback({
        status: Status.NotInSession,
        id: item.id
      })
    }
    else {
      const sessionId = getUserSession(socket);
      if (item.type == ItemType.Note) {
        (<Note>item).selected = socket.id;
      }
      if (!whiteboards.get(sessionId).has(item.id)) {
        whiteboards.get(sessionId).set(item.id, item);
        socket.to(sessionId).emit('item', item, item.id);
        callback({
          status: Status.Ok,
          id: item.id
        });
      } else {
        callback({
          status: Status.NotAuthorized,
          id: item.id
        });
      }
    }
  });

  socket.on('deleteItem', itemID => {
    if (socket.rooms.size == 1) {
      socket.emit('requestFailed', "You are not in a session!", Status.NotInSession);
    }
    else {
      const sessionId = getUserSession(socket);
      whiteboards.get(sessionId).delete(itemID);
      socket.to(sessionId).emit('itemDeleted', itemID);
    }
  });

  socket.on('selectItem', (itemID, callback) => {
    const sessionId = getUserSession(socket);
    const item = <Note>whiteboards.get(sessionId).get(itemID);
    if (item.selected != "") {
      callback({
        granted: false,
        user: item.selected
      });
    }
    else if (whiteboards.get(sessionId).has(itemID)) {
      item.selected = socket.id;
      socket.to(sessionId).emit('itemSelected', itemID, socket.id);
      callback({
        granted: true,
        user: socket.id
      });
    }
    else {
      callback({
        granted: false,
        user: null
      })
    }
  });

  socket.on('editItem', (itemID, text) => {
    const sessionId = getUserSession(socket);
    const whiteboard = whiteboards.get(sessionId);
    if (whiteboard.has(itemID)) {
      const item = whiteboard.get(itemID);
      switch (item.type) {
        case ItemType.Note:
          if ((<Note>item).selected != socket.id) {
            socket.emit('requestFailed', "You must select the item first", Status.ItemNotSelected);
          }
          else {
            (item as Note).text = text;
            io.to(sessionId).emit('noteText', itemID, text);
          }
          break;
        case ItemType.Comment:
          const comment = <Comment>item;
          if (comment.author.id == socket.id) {
            comment.text = text;
            io.to(sessionId).emit('noteText', itemID, text);
          } else {
            socket.emit('requestFailed', "This is not your comment", Status.NotAuthorized);
          }
          break;
        default:
          socket.emit('requestFailed', "Item is not a note or a comment", Status.ItemNotEditable);
          break;
      }
    }
    else {
      socket.emit('requestFailed', "Item not found", 26);
    }
  });

  socket.on('updateDrawing', (itemID, edge) => {
    if (socket.rooms.size > 1) {
      const sessionId = getUserSession(socket);
      if (whiteboards.has(sessionId)) {
        const whiteboard = whiteboards.get(sessionId);
        if (whiteboard.has(itemID)) {
          const item: Drawing = whiteboard.get(itemID) as Drawing;
          item.edges.push(edge);
          whiteboard.set(itemID, item);
          socket.to(sessionId).emit('updateDrawing', itemID, edge);
        }
      } else {
        console.error(`Client ${socket.id} is in session ${sessionId}, but the session does not have a whiteboard`);
      }
    } else {
      socket.emit('requestFailed', "Not in session", Status.NotInSession);
    }
  });

  // TODO: can we somehow not repeat all this
  socket.on('moveNote', (itemID, position) => {
    const sessionId = getUserSession(socket);
    const whiteboard = whiteboards.get(sessionId);
    if (whiteboard.has(itemID)) {
      const item = whiteboard.get(itemID);
      if (item.type == ItemType.Note) {
        if ((<Note>item).selected != socket.id) {
          socket.emit('requestFailed', "You must select the item first", Status.ItemNotSelected);
        }
        else {
          (item as Note).pos = position;
          socket.to(sessionId).emit('notePosition', itemID, position);
        }
      }
      else {
        socket.emit('requestFailed', "Item is not a note", Status.ItemNotEditable);
      }
    }
    else {
      socket.emit('requestFailed', "Item not found", 26);
    }
  });

  socket.on('releaseNote', itemID => {
    if (socket.rooms.size > 1) {
      const session = getUserSession(socket);
      if (whiteboards.has(session)) {
        if (whiteboards.get(session).has(itemID)) {
          if (whiteboards.get(session).get(itemID).type == ItemType.Note) {
            io.to(session).emit('itemReleased', itemID);
            (<Note>whiteboards.get(session).get(itemID)).selected = '';
          } else {
            socket.emit('requestFailed', "Item is not a note", Status.ItemNotEditable);
          }
        } else {
          socket.emit('requestFailed', "Item not found", Status.ItemNotFound);
        }
      } else {
        socket.emit('requestFailed', "You are somehow in a session that doesn't exist. Something has gone quite wrong.", Status.NotInSession);
      }
    } else {
      socket.emit('requestFailed', "You are not in a session", Status.NotInSession);
    }
  });

  socket.on('endSession', async () => {
    await leaveSession(socket);
  });

  // the requirements demand that the session ends if the host loses connection for some reason
  socket.on('disconnecting', async () => {
    console.log("A user disconnected");
    if (socket.rooms.has(userToSession(socket.id))) {
      await leaveSession(socket);
    }
  });
})

io.of("/").adapter.on("join-room", async (room, id) => {
  const user = (await io.in(id).fetchSockets())[0];
  if (user.rooms.size > 2) {
    user.leave(room);
    user.emit('requestFailed', "You can only join one session", Status.AlreadyInSession);
  }
});

server.listen(port, () => {
  console.log(`Listening on *:${port}`)
})
