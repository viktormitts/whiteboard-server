import User from './User';

export enum ItemType {
    Drawing = 1,
    Note,
    Image,
    Comment
}

export interface Item {
    type: ItemType
    id: string
}

export interface Drawing extends Item {
    strokeStyle: string
    lineWidth: number
    lineCap: CanvasLineCap
    globalCompositeOperation: string
    edges: [Vec2, Vec2][]
}

export interface Note extends Item {
    text: string
    pos: Vec2
    selected: string
}

export interface Image extends Item {
    data: string
    pos: Vec2
    comments: Map<string, Comment>
}

export interface Comment extends Item {
    text: string
    author: User
    selected: boolean
    imageId: string
}

//

export interface Vec2 {
    x: number
    y: number
}
